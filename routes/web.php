<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', [UserController::class, 'profileShow'])->middleware(['auth'])->name('dashboard');

Route::get('/reports/user', [UserController::class, 'getReportUser'])->middleware(['auth','admin']);;

Route::resource('users', UserController::class)->middleware(['auth','admin']);

Route::patch('/profile/{user}', [UserController::class, 'updateUser'])->name('profile.updateUser')->middleware(['auth']);

Route::resource('posts', PostController::class)->middleware(['auth']);



require __DIR__.'/auth.php';
