<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('User Information') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="bg-light p-4 rounded">
                    <h1>Show user</h1>
                    <div class="lead">

                    </div>

                    <div class="container mt-4">
                        <div>
                            ID: {{ $user->id }}
                        </div>
                        <div>
                            Name: {{ $user->first_name }}
                        </div>
                        <div>
                            Email: {{ $user->email }}
                        </div>

                    </div>

                </div>
                <div class="mt-4">
                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info">Edit</a>
                    <a href="{{ route('users.index') }}" class="btn btn-default">Back</a>
                </div>


            </div>
        </div>
    </div>
</x-app-layout>
