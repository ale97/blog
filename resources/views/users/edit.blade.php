<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('User Information') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="bg-light p-4 rounded">
                    <h1>Update user</h1>
                    <div class="lead">

                    </div>

                    <div class="container mt-4">
                        <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />    
                                 
                        <form method="post" action="{{ route('users.update', $user->id) }}">
                            @method('patch')
                            @csrf
                            <div class="mb-3">
                                <label for="first_name" class="form-label">Name</label>
                                <input value="{{ $user->first_name }}" type="text" class="form-control"
                                    name="first_name" placeholder="Name" required>

                                @if ($errors->has('first_name'))
                                <span class="text-danger text-left">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="last_name" class="form-label">Last Name</label>
                                <input value="{{ $user->last_name }}" type="text" class="form-control" name="last_name"
                                    placeholder="Last Name" required>

                                @if ($errors->has('last_name'))
                                <span class="text-danger text-left">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input value="{{ $user->email }}" type="email" class="form-control" name="email"
                                    placeholder="Email address" required>
                                @if ($errors->has('email'))
                                <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="role" class="form-label">Role</label>
                                <select class="form-control" id="type" name="role">
                                    @foreach($roles as $role)
                                    <option value="{{ $role->id }}" {{ $role->id == $user->role ? 'selected' : '' }}>
                                        {{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role'))
                                <span class="text-danger text-left">{{ $errors->first('role') }}</span>
                                @endif
                            </div>

                            <!-- Password -->
                            <div class="mt-4">
                                <x-label for="password" :value="__('Password')" />

                                <x-input id="password" class="block mt-1 w-full" type="password" name="password"
                                     autocomplete="new-password" required/>
                            </div>

                            <button type="submit" class="btn btn-primary">Update user</button>
                            <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</button>
                        </form>
                    </div>

                </div>


            </div>
        </div>
    </div>
</x-app-layout>
