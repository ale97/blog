<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('User List') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">


                    <div class="bg-light p-4 rounded">
                        <h1>Users</h1>
                        <div class="lead">

                            <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm float-right">Add new
                                user</a>
                        </div>


                        <input class="form-control" id="myInput" type="text" placeholder="Search..">
                        <br>

                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th scope="col" width="1%">#</th>
                                    <th scope="col" width="15%">Full Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col" width="10%">Roles</th>
                                    <th scope="col" width="1%" colspan="3">Option</th>
                                </tr>
                            </thead>
                            <tbody id="userTable">
                                @foreach($users as $user)
                                <tr>
                                    <th scope="row">{{ $user->id }}</th>
                                    <td>{{ $user->first_name .' '. $user->last_name  }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if( $user->role == 1 )
                                        Admin
                                        @else
                                        Blogger
                                        @endif

                                    </td>

                                    <td><a href="{{ route('users.show', $user->id) }}"
                                            class="btn btn-warning btn-sm">Show</a></td>
                                    <td><a href="{{ route('users.edit', $user->id) }}"
                                            class="btn btn-info btn-sm">Edit</a></td>
                                    <td>
                                        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy',
                                        $user->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{$users->links() }}

                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $(document).ready(function () {
        $("#myInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#userTable tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });

</script>
