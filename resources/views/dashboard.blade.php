<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <div class="px-4">

                        <div class="font-medium text-base text-gray-800">
                            <x-label for="name" :value="__('First Name:')" />
                            {{ Auth::user()->first_name }} </div>

                        <div class="font-medium text-base text-gray-800">
                            <x-label for="name" :value="__('Last Name:')" />
                            {{ Auth::user()->last_name }}</div>

                        <div class="font-medium text-sm text-gray-500">
                            <x-label for="email" :value="__('Email:')" />
                            {{ Auth::user()->email }}</div>
                        <div class="font-medium text-sm text-gray-500">
                            <x-label for="name" :value="__('Last Login:')" />
                            {{ Auth::user()->last_login_at }}</div>
                    </div>

                </div>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#profileEdit">
                    Edit User Information
                </button>

                <div class="px-1">
                    @if (Auth::user()->isAdmin())
                    <div>
                        <canvas id="cantUserChart"></canvas>
                    </div>
                    @endif
                </div>

                <div class="px-1">
                    @if (Auth::user()->isAdmin())
                    <label for="">Total number of Post:</label>
                    <label for="">{{$postCantAll}}</label>
                    @endif
                    <br>
                    <label for="">Post you have created:</label>
                    <label for="">{{$postCantByUser}}</label>
                </div>



                <!-- Modal -->
                <div class="modal fade" id="profileEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"> Edit User Information</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">


                                <div class="container mt-4">
                                    <form data-action="{{ route('profile.updateUser', $user->id) }}" method="post"
                                        enctype="multipart/form-data" id="updateUser">
                                        @method('patch')
                                        @csrf
                                        <div class="mb-3">
                                            <label for="first_name" class="form-label">Name</label>
                                            <input value="{{ $user->first_name }}" type="text" class="form-control"
                                                name="first_name" placeholder="Name" required>

                                            @if ($errors->has('first_name'))
                                            <span
                                                class="text-danger text-left">{{ $errors->first('first_name') }}</span>
                                            @endif
                                        </div>
                                        <div class="mb-3">
                                            <label for="last_name" class="form-label">Last Name</label>
                                            <input value="{{ $user->last_name }}" type="text" class="form-control"
                                                name="last_name" placeholder="Last Name" required>

                                            @if ($errors->has('last_name'))
                                            <span class="text-danger text-left">{{ $errors->first('last_name') }}</span>
                                            @endif
                                        </div>

                                        <div class="mb-3">
                                            <label for="email" class="form-label">Email</label>
                                            <input value="{{ $user->email }}" type="email" class="form-control"
                                                name="email" placeholder="Email address" required>
                                            @if ($errors->has('email'))
                                            <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>

                                        @if(Auth::user()->role == 1 )
                                        <div class="mb-3">
                                            <label for="role" class="form-label">Role</label>
                                            <select class="form-control" id="type" name="role">
                                                @foreach($roles as $role)
                                                <option value="{{ $role->id }}"
                                                    {{ $role->id == $user->role ? 'selected' : '' }}>{{ $role->name }}
                                                </option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('role'))
                                            <span class="text-danger text-left">{{ $errors->first('role') }}</span>
                                            @endif
                                        </div>
                                        @endif

                                        <!-- Password -->
                                        <div class="mt-4">
                                            <x-label for="password" :value="__('Password')" />

                                            <x-input id="password" class="block mt-1 w-full" type="password"
                                                name="password" required autocomplete="new-password" />
                                        </div>

                                        <button type="submit" class="btn btn-primary">Update user</button>
                                        <a href="{{ route('dashboard') }}" class="btn btn-default">Cancel</button>
                                    </form>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</x-app-layout>
@if (Auth::user()->role == 1 )
<script src="{{ asset('js/chart.js') }}" defer></script>
@endif
