<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Post List') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">


                    <div class="bg-light p-4 rounded">
                        <h1>Posts</h1>
                        <div class="lead">

                            <a href="{{ route('posts.create') }}" class="btn btn-primary btn-sm float-right">Add new
                                post</a>
                        </div>


                        <input class="form-control" id="myInput" type="text" placeholder="Search..">
                        <br>

                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th scope="col" width="1%">#</th>
                                    <th scope="col" width="15%">Name</th>
                                    <th scope="col">Description</th>
                                    <th scope="col" width="10%">Created by</th>
                                    <th scope="col" width="10%">Date</th>
                                    <th scope="col" width="1%" colspan="3">Option</th>
                                </tr>
                            </thead>
                            <tbody id="userTable">
                                @foreach($posts as $post)
                                <tr>
                                    <th scope="row">{{ $post->id }}</th>
                                    <td>{{ $post->name}}</td>
                                    <td>{{ $post->description }}</td>
                                    <td>{{ $post->created_by }}</td>
                                    <td>{{ $post->date }}</td>

                                    @if($post->userId == Auth::user()->id || Auth::user()->role == 1 )

                                    <td><a href="{{ route('posts.show', $post->id) }}"
                                            class="btn btn-warning btn-sm">Show</a></td>
                                    <td><a href="{{ route('posts.edit', $post->id) }}"
                                            class="btn btn-info btn-sm">Edit</a></td>
                                    <td>
                                        {!! Form::open(['method' => 'DELETE','route' => ['posts.destroy',
                                        $post->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                    @else
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    @endif

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $posts->links() !!}

                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
{{-- search in the table --}}
<script>
    $(document).ready(function () {
        $("#myInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#userTable tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
