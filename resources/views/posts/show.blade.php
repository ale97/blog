<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Post Details') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="bg-light p-4 rounded">
                    <h1>Post by {{ $post->created_by }}</h1>
                    <div class="lead">

                    </div>

                    <div class="container mt-4">
                        <div>
                            Created by: {{ $post->created_by }}
                        </div>
                        <div>
                            Date: {{ $post->date }}
                        </div>
                        <div>
                            Name: {{ $post->name }}
                        </div>
                        <div>
                            Description: {{ $post->description }}
                        </div>

                    </div>

                </div>
                <div class="mt-4">
                    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-info">Edit</a>
                    <a href="{{ route('posts.index') }}" class="btn btn-default">Back</a>
                </div>


            </div>
        </div>
    </div>
</x-app-layout>
