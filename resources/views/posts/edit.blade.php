<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Post Information') }}
        </h2>
    </x-slot>

    <div class="py-12">

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="bg-light p-4 rounded">
                    <h1>Update post</h1>
                    <div class="lead">

                    </div>

                    <div class="container mt-4">
                        <x-auth-validation-errors class="mb-4" :errors="$errors" /> 
                        
                        <form method="POST" action="{{ route('posts.update', $post->id) }}">
                            @method('patch')
                            @csrf

                            <!-- titule -->
                            <div>
                                <x-label for="name" :value="__('Name')" />

                                <x-input id="name" class="block mt-1 w-full" type="text" name="name"
                                    :value="$post->name" required autofocus />
                            </div>

                            <!-- description -->
                            <div>
                                <x-label for="description" :value="__('Description')" />

                                <x-input id="description" class="block mt-1 px-3 py-1.5 text-base w-full " type="text"
                                    name="description" :value="$post->description" required autofocus />
                            </div>

                            <!-- created_by -->
                            <div>
                                <x-label for="created_by" :value="__('Created by')" />

                                <label for="">{{$post->created_by}}</label>

                                
                            </div>

                            <!-- date -->
                            <div>
                                <x-label for="date" :value="__('Date')" />
                                <x-label for="date" :value="$post->date" />
                                <x-label for="date" :value="__('New Date')" />

                                <x-input id="date" class="block mt-1 w-full" type="date" name="date"
                                    :value="$post->date"  autofocus />
                            </div>

                            <div class="flex items-center justify-end mt-4">
                                <x-button class="ml-4">
                                    {{ __('Update') }}
                                </x-button>
                                <a href="{{ route('posts.index') }}" class="btn btn-default">Back</a>
                            </div>
                        </form>

                    </div>

                </div>


            </div>
        </div>
    </div>
</x-app-layout>
