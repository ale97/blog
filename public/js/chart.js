var charts = {
    getUser: function () {
        return $.get('/reports/user');
    }

};

let cantAdmin = 0;
let cantBlogger = 0;

function getUserData() {

    charts.getUser().done(function (json) {

        json.users.forEach(function (users) {
            if (users.role == 1) {
                cantAdmin++;
            } else {
                cantBlogger++;
            }
        });

        const labels = [
            'Administrador',
            'Blogguer',
        ];

        const data = {
            labels: labels,
            datasets: [{
                label: 'Users by User Types',
                backgroundColor: 'rgb(130, 155, 215)',
                borderColor: 'rgb(255, 99, 132)',
                data: [cantAdmin, cantBlogger],
            }]
        };

        const config = {
            type: 'bar',
            data: data,
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            },
        };

        const myChart = new Chart(
            document.getElementById('cantUserChart'),
            config
        );

    })
};


$(document).ready(function () {

    getUserData();
});
