<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->first_name = 'Alejandro';
        $user->last_name = 'Aguirre';
        $user->email = '123@gmail.com';
        $user->password = Hash::make('123');
        $user->role = 1;

        $user->save();
    }
}