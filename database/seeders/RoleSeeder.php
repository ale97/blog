<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role = new Role();
      $role->name = 'Admin';
      $role->description = 'Administrator';
      $role->save();

      $role = new Role();
      $role->name = 'Blogger';
      $role->description = 'Blogger';
      $role->save();
    }
}
